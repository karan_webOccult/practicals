<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pattern 1</title>
</head>
<body>
    <form action='#' method='POST'>
        <input type="text" name='input'>
        <button type='submit'>Submit</button>
    </form>
</body>
</html>

<?php
 if($_POST){

    //Intializing the n value 
    $n = intval($_POST['input']);    
    $table = '<table>';
    for ($i=0; $i < $n ; $i++) {         
        $table.='<tr>';                   
        for ($j=-($n*2 - 1); $j < ($n*2) ; $j++) {                                
                if( $i <= abs(abs($j) - abs($n*2 - 1)) && abs(abs($j) - abs($n*2 - 1) + $i) <= $i && $i <ceil($n/2)  ){                      
                        $table.='<td>* </td>';                     
                }else{                    
                    if(abs($j) <= $i || ($i >=ceil($n/2) && abs(abs($j) - abs($n*2 - 1)) >= ceil($n/2)) ){                                                
                        $table.='<td>* </td>'; 
                    }else{          
                        //Adding space for proper presentation           
                        $table.='<td></td>';      
                    }
                }            
        }   
        $table.='</tr>';                     
    }    
    $table.='</table>';
    echo $table;
}
   
?>