
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pattern 1</title>
</head>
<body>
    <form action='#' method='POST'>
        <input type="text" name='input'>
        <button type='submit'>Submit</button>
    </form>
</body>
</html>

<?php
    if($_POST){

        //Intializing the n value 
        $n = intval($_POST['input']);   

        for ($i=0; $i < $n ; $i++) {                             
            if( $i < $n - 9 ){

                for ($k=0; $k < $n ; $k++) {                     
                    if($k < $i ){
                        echo '&nbsp;&nbsp;';
                    }
                }           

            } 

            if($i >= $n - 9){
                //Adding space for proper presentation               
                echo str_repeat('&nbsp;&nbsp;',$n - 9);
            }

            for ($j=-($n); $j <= ($n) ; $j++) {                                
            
                if($j==0){
                    continue;
                }       
                if(abs($j) <= $i){
                    echo '*';
                }else{                             
                    echo abs($n - abs($j) + 1); 
                }                                       
            }   
            //Breaking the line 
            echo ' <br> ';    
        }    
    }
?>