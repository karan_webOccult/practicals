<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pattern 1</title>
</head>
<body>

    <form action='#' method='POST'>
        <input type="text" name='input'>
        <button type='submit'>Submit</button>
    </form>

</body>
</html>

<?php
    if($_POST){

        //Intializing the n value 
        $n = intval($_POST['input']);
        $table = '<table>';

        for ($i=(-$n); $i <= $n ; $i++) {         
            $table.='<tr>';                   
            for ($j=-($n); $j <= $n ; $j++) {                        
                if($j==0){
                   continue; 
                }else if(abs($j) <= abs($i) ) { 
                    //Adding space for proper presentation                        
                    $table.='<td></td>';  
                }else{                
                    $table.='<td>*</td>';  
                }
            }   
            $table.='</tr>';                     
        }    

        $table.='</table>';
        echo $table;
    }
   
?>