<?php

function insertion_sort($arr)
{
    for ($i = 1; $i < count($arr); $i++)
    {
        $key = $arr[$i];//taking the intial element as a key for comparision
        $j = $i-1;

        //Comparing the current element with the key
        while ($j >= 0 && $arr[$j] > $key)
        {
            //Swaping the next element with the current element
            $arr[$j + 1] = $arr[$j];            
            $j = $j - 1;
        }
        //Duplicate elements will be replaced by the key
        $arr[$j + 1] = $key;
    }
    return $arr;
}

$arr = array(12, 11, 13, 5, 6);
echo implode(' ',insertion_sort($arr));

 
?>