
<?php 
function selection_sort($arr)
{
    for ($i = 0; $i < count($arr); $i++)
    {
        for ($j = $i; $j < count($arr); $j++)
        {   
            //comparing the current value of i with current value of j             
            if($arr[$i] > $arr[$j]){                
                 //Swap the array by assigning the values
                list($arr[$j],$arr[$i]) = array($arr[$i],$arr[$j]);
            }
        }                  
    }
    return $arr;
}

$arr = array(20, 12, 10, 15, 2);
echo implode(' ',selection_sort($arr));

 
?>