<?php

//Intialing the unsorted array
$a = array(15,10,25,85,4,1,2,78,9);

function qsort(array &$a, $low, $high)
{

    //Comparing the values until the array get's sorted
    if ($low < $high){        
        $pivot = partition($a, $low, $high);            
        qsort($a, $low, $pivot - 1);
        qsort($a, $pivot + 1, $high);
    }

}

function partition(array &$a, $left, $right)
{
    //Finding the pivot element.
    $pivot = (int)($left + $right)/2;
    $pivVal = $a[$pivot];

    //Swaping the pivot with right so that each value can be compared with element    
    list($a[$pivot],$a[$right]) = array($a[$right],$a[$pivot]);
    $index = $left;

    for ($i = $left; $i < $right; $i++) {
        //Comparing each element from the left to right with the pivot element
        if ($a[$i] < $pivVal) {
            //Swaping the value which are less than the pivot value
            list($a[$i],$a[$index++]) = array($a[$index],$a[$i]);            
        }
    }
    //Swaping the last element with the current index    
    list($a[$index],$a[$right]) = array($a[$right],$a[$index]);
    //returing the current index to further determine the values of mid,left and right
    return $index;
}

function swap(array &$a, $i, $j)
{
    $swap = $a[$i];
    $a[$i] = $a[$j];
    $a[$j] = $swap;
}


qsort($a, 0, count($a)-1);
echo implode(' ',$a);

?>