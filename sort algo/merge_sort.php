<?php

//Intialing the unsorted array
$unsorted_numbers = array(3, 7, 1, 26);

function merge_arrays($left_arr, $right_arr)
{
    /*
     Merge:
        This function is mainly used to compare the values
        and storing the minimun value in a new array
    */ 
    $res = array();
    
    while (count($left_arr) > 0 && count($right_arr) > 0) {
        
        //Compare the left array with right array when none of the arrays are empty.

        if ($left_arr[0] > $right_arr[0]) {
            $res[] = $right_arr[0];
            $right_arr = array_slice($right_arr, 1);
        } else {
            $res[] = $left_arr[0];
            $left_arr = array_slice($left_arr, 1);
        }
    }

    //Intialize the value of left array to a new array if the left array is not empty
    while (count($left_arr) > 0) {
        $res[] = $left_arr[0];
        //Deducting the last element from left array
        $left_arr = array_slice($left_arr, 1);
    }

    //Intialize the value of right array to a new array if the right array is not empty
    while (count($right_arr) > 0) {
        $res[] = $right_arr[0];
        //Deducting the last element from right array
        
        $right_arr = array_slice($right_arr, 1);
    }

    
    return $res;

}

function mergesort($array)
{
     /*
    Divide:
        This funtion wil be called recursively until only one element is left.
        This will mainly divide the array until only one element is left
            
    */ 
    //Returing the array when only one element is left
    if (count($array) == 1) return $array;
    
    //Find the mid value
    $mid = count($array) / 2;
    //Divind the array from 0 to mid and storing the values in the left array
    $left_arr = array_slice($array, 0, $mid);

    //Divind the array from mid to end of the array and storing the values in the right array
    $right_arr = array_slice($array, $mid);

    //Calling the functions recursively until only one element is left
    $left_arr = mergesort($left_arr);
    $right_arr = mergesort($right_arr);   

    //Calling the function for sorting and merging the array 
    return merge_arrays($left_arr, $right_arr);

}

$sorted_numbers = mergesort($unsorted_numbers);
print_r($sorted_numbers);
