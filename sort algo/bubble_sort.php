
<?php
function bubble_sort($arr) {
    //count the array size
    $size = count($arr)-1;
    for ($i=0; $i<$size; $i++) {
        for ($j=0; $j<$size-$i; $j++) {            
            //Compare the value            
            if ($arr[$j+1] < $arr[$j]) {                
                //Swap the array by assigning the values
                list($arr[$j], $arr[ $j+1]) = array($arr[ $j+1], $arr[$j]);
            }
        }

    }
    return $arr;
}
$arr = array(5,4,3,2,1);

echo implode(' ',bubble_sort($arr));


?>