<?php
if(isset($_SESSION['username'])){
    header('location:home.php');
  }else{
      include 'auth.php';
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign In</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<form action='<?= $_SERVER['PHP_SELF'] ?>' method='post'>  
    <div class="container">
      <h1>Sign In</h1>      

      <label for="email"><b>Email</b></label>
      <span class='errorMssg'><?=(isset($emailError)) ? $emailError : '' ; ?></span>
      <input type="text" placeholder="Enter Email" name="email" value="<?= (isset($_POST['email']) && empty($emailError) && isset($nameError)) ? $_POST['email'] :'' ?>">

      <label for="psw"><b>Password</b></label>
      <span class='errorMssg'><?=(isset($pssdError)) ? $pssdError :''; ?></span>    
      <input type="password" placeholder="Enter Password" name="psw"value="<?= (isset($_POST['psw']) && empty($pssdError) && isset($pssdError)) ? $_POST['psw'] :'' ?>" >      

      <div class="clearfix">
        <button type="submit" class="btn" name='signIn'>Sign In</button>
        <div class='button'><a href='index.php' class="btn" value='sign_in'>Sign Up</a></div>
      </div>
      <?php echo $userMessage; ?>
    </div>
  </form>
</body>
</html>