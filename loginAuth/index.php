<?php
include 'auth.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Simple Reistration Form</title>
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <form action='<?= $_SERVER['PHP_SELF'] ?>' method='post' enctype="multipart/form-data">
    <div class="container">

      <h1>Sign Up</h1>
      <p>Please fill in this form to create an account.</p>

      <label for="username"><b>Username</b></label>
      <span class='errorMssg'><?= (isset($nameError)) ? $nameError : ''; ?></span>
      <input type="text" name="username" placeholder="Enter Username" value="<?= (isset($_POST['username']) && empty($nameError) && isset($nameError)) ? $_POST['username'] : '' ?>">

      <label for="email"><b>Email</b></label>
      <span class='errorMssg'><?= (isset($emailError)) ? $emailError : ''; ?></span>
      <input type="text" placeholder="Enter Email" name="email" value="<?= (isset($_POST['email']) && empty($emailError) && isset($nameError)) ? $_POST['email'] : '' ?>">

      <label for="psw"><b>Password</b></label>
      <span class='errorMssg'><?= (isset($pssdError)) ? $pssdError : ''; ?></span>
      <input type="password" placeholder="Enter Password" name="psw" value="<?= (isset($_POST['psw']) && empty($pssdError) && isset($pssdError)) ? $_POST['psw'] : '' ?>">

      <label for="phone"><b>Phone Number</b></label>
      <span class='errorMssg'><?= (isset($phoneError)) ? $phoneError : ''; ?></span>
      <input type="number" name="phone" placeholder="812345678" value="<?= (isset($_POST['phone']) && empty($phoneError) && isset($phoneError)) ? $_POST['phone'] : '' ?>">

      <label for="profile_pic"><b>Upload Profile Picture</b></label>
      <input type='file' name='profilePic'>
      <span class='errorMssg'><?= (isset($msg) && !empty($msg)) ? $msg : ''; ?></span>
      <?php if (isset($error) && $error == 0) {
      ?>
        <div class="success" style="width: 100%;">
          <div>
            <p> Congratulations ! Registration is successful </p>
          </div>
        </div>
      <?php
      } ?>

      <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>
      <div class="clearfix">
        <button type="submit" class="btn" name='signUp'>Sign Up</button>
        <div class='button'><a href='sign_in.php' class="btn" value='signIn'>Sign In</a></div>
      </div>

    </div>
  </form>
</body>

</html>