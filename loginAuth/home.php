<?php
include 'auth.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign In</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<form action='#' method='post'>  
    <div class="container">

      <h1>Welcome <?= $_SESSION['username']?> !</h1>    
      <button type="submit" class="btn" name='logOut'>Log Out</button>
      
    </div>
  </form>
</body>
</html>