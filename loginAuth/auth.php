<?php
session_start();

$emailError = $nameError = $pssdError = $phoneCodeError = $phoneError = $userMessage = '';

if (isset($_POST['signUp'])) {
  $flag = 0;
  if (isset($_POST['username'])) {
    if (empty($_POST['username'])) {
      $nameError = 'Username is required';
    } else {
      $nameError = (!preg_match("/^[a-zA-Z-' ]*$/", $_POST['username'])) ? 'No special characters are allowed !' : '';
    }
    if (!empty($nameError)) {
      $flag = 1;
    }
  }

  if (isset($_POST['email'])) {
    if (empty($_POST['email'])) {
      $emailError = 'Email is required';
    } else {
      $emailError =  (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) ?  'Invalid email format !' : '';
    }
    if (!empty($emailError)) {
      $flag = 1;
    }
  }

  if (isset($_POST['psw'])) {
    if (empty($_POST['psw'])) {
      $pssdError = 'Password is required';
    } else {
      $pssdError = !preg_match("/^[a-z0-9]{4,}$/", $_POST['psw']) ? 'Minimum 8 characters are required!' : '';
    }
    if (!empty($pssdError)) {
      $flag = 1;
    }
  }

  if (isset($_POST['phone'])) {
    if (empty($_POST['phone'])) {
      $phoneError = 'Phone is required';
    } else {
      $phoneError = !preg_match("/^[0-9]{10}$/", $_POST['phone']) ? 'Numbers should be equal to 10' : '';
    }
    if (!empty($phoneError)) {
      $flag = 1;
    }
  }

  if ($flag == 0) {

    if (!empty($_FILES['profilePic']['name']) && isset($_FILES['profilePic'])) {

      $directory = 'uploads/';
      $file_path = $directory . basename($_FILES['profilePic']['name']);
      $extension = pathinfo($file_path, PATHINFO_EXTENSION);
      $allowedExtensions = ['jpg', 'png', 'gif', 'jpeg'];
      $error = 0;
      $msg = '';
      $isImage = getimagesize($_FILES['profilePic']['tmp_name']);

      if(file_exists($file_path)){
        $error = 1;
        $msg = 'File already exists.Please upload a new file.';
      }

      if (empty($isImage)) {
        $error = 1;
        $msg = 'Uploaded file is not an image please upload an image file';
      }

      if (!in_array($extension, $allowedExtensions)) {
        $error = 1;
        $msg = 'Only JPG,PNG and GIF extensions are allowed';
      }

      //2000000 = 2mb
      if ($_FILES['profilePic']['size'] > 2000000) {
        $error = 1;
        $msg = 'Please upload file less than 2MB';
      }

      if ($error == 0) {
        if (!move_uploaded_file($_FILES['profilePic']['tmp_name'], $file_path)) {
          $error = 1;
          $msg = 'Something went Wrong.Please reupload the file';
        } else {
          $file = fopen("uploads/userData.txt", "a") or die("Unable to open file!");
          fwrite($file,PHP_EOL.json_encode($_POST));          
        }
      }      
    } else {
      $msg = 'Profile pic is required';
    }
  }
}

if ( isset($_POST['signIn'])) {
  $validUser = 0;
  $flag = 0;
  if (isset($_POST['email'])) {
    if (empty($_POST['email'])) {
      $emailError = 'Email is required';
    } else {
      $emailError =  (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) ?  'Invalid email format !' : '';
    }
    if (!empty($emailError)) {
      $flag = 1;
    }
  }

  if (isset($_POST['psw'])) {
    if (empty($_POST['psw'])) {
      $pssdError = 'Password is required';
    } else {
      $pssdError = !preg_match("/^[a-z0-9]{4,}$/", $_POST['psw']) ? 'Minimum 8 characters are required!' : '';
    }
    if (!empty($pssdError)) {
      $flag = 1;
    }
  }
  if ($flag == 0) {

    $file = fopen('uploads/userData.txt', 'r');

    while (!feof($file)) {
      $tmpArr = json_decode(fgets($file), true);
      if ($tmpArr['email'] == $_POST['email'] && $tmpArr['psw'] == $_POST['psw'] && !empty($tmpArr) ) {
        $validUser = 1;
        $_SESSION['username'] = $tmpArr['username'];
        break;
      }
    }
    $userMessage = ($validUser) ? 'You are an authorised user' : 'You are not an authorised user';
    if($validUser){
      header('location:home.php');
    }
    fclose($file);
  }
}

if(isset($_POST['logOut'])){

  unset($_SESSION['username']);
  header('location:sign_in.php');

}