<?php 
function getFiles($dir){       

    $res['folder'] = [];
    $res['files'] = [];    

    $files = array_diff(scandir($dir),array('.','..'));        
    $extensionAllowed=['txt','jpeg','jpg','png','xls','zip'];

    

    foreach( $files as $key => $value ){
        
        $temp = [];
        $temp['name'] =  $value;
        $temp['path'] =  $dir.'/'.$value;   

        if(!is_dir($dir . '/' . $value ) ){  
            if(array_search( pathinfo($dir . '/' . $value,PATHINFO_EXTENSION) ,$extensionAllowed) !== false ){                
                $temp['type'] =  $extensionAllowed[array_search( pathinfo($dir . '/' . $value,PATHINFO_EXTENSION) ,$extensionAllowed)];                
                array_push($res['files'],$temp);
            }                            
        }else{                            
                $temp['type'] =  'folder';    
                array_push($res['folder'],$temp);            
        }                
    }
    return $res;
}
 
if(isset($_POST['dirname'])){
    $res = getFiles($_POST['dirname']);    
    if( strpos(realpath($_POST['dirname']),'root')){        
        $response['status'] = true;
        $response['message'] = 'Data retrieved successfully';
        $response['data'] = $res;
    }else{
        $response['status'] = true;
        $response['message'] = 'You don\'t have right to access the directory';
        $response['data'] = [];
    }    
    
}else{
    $response['status'] = false;
    $response['message'] = 'Something went wrong';
    $response['data'] = [];
}   

header('Content-Type: application/json');
echo json_encode($response); 
?>