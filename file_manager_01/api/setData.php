<?php
require('functions.php');

if(isset($_POST) && in_array($_POST['method'],array('move','copy','delete')) ){
    
    $files = json_decode($_POST['name'],true);          
    $destinationPath = $_POST['dirname'];        
    $flag = true;
    if(in_array($_POST['method'],array('move','copy')) && is_dir($files[0]['path']) ){
        
        if (strpos($destinationPath, $files[0]['path']) !== false) {
            
            $response['error'] = false;
            $response['message'] = 'The destination folder is a subfolder of the source folder';
            echo json_encode($response);
            return;
        }

    }
    $fileName = [];
    foreach( $files as $value ){
        $tmp = explode('/',$value['path']);                
        
        if(in_array($_POST['method'],array('move','copy'))){
            if($_POST['method'] == 'move') {
                $flag =  rename($value['path'], $destinationPath .'/'. $tmp[ count($tmp) - 1 ] );
            }else{
                 array_push($fileName, CopyDir($value['path'], $destinationPath .'/'. $tmp[ count($tmp) - 1 ] ));
                 $flag = true;
            }  
        }else{
            $filePath = $destinationPath.'/'.$tmp[ count($tmp) - 1 ];
            
            if(is_dir($filePath)){              
                $flag = deleteDir($filePath);                       
            }else{
                if(file_exists($filePath)){
                    $flag = unlink( $filePath );   
                }
            }                       
        }      
    }

    if($flag){
        $response['error'] = false;
        $response['message'] = 'Opeation successful!';
        $response['data'] = $fileName;
    }else{
        $response['error'] = true;
        $response['message'] = 'Something went wrong.';        
    }
    
    header('Content-Type: application/json');
    echo json_encode($response);

}

if(isset($_POST) && in_array($_POST['method'],array('rename')) ){

    $filterText = "/^[a-zA-Z0-9()' ]*$/";   
    $files = json_decode($_POST['name'],true);           
    $ext =  !is_dir($files[0]['path'])  ? '.'.pathinfo($files[0]['path'], PATHINFO_EXTENSION ) : '';
    $destinationPath = $_POST['dirname'].'/'.$_POST['newName'] .$ext ;        
    if(!preg_match($filterText, $_POST['newName'])){        
        $response['error'] = true;
        $response['message'] = 'No special characters are allowed !';
    }else{
        if( file_exists($destinationPath) ){
            $destinationPath = rename_path($destinationPath);
        }
        if(rename($files[0]['path'],trim($destinationPath))){
            $response['error'] = false;
            $response['message'] = 'Opeation successful!';            
        }else{
            $response['error'] = true;
            $response['message'] = 'Something went wrong.';
        }
    }    
    header('Content-Type: application/json');
    echo json_encode($response);
}

?>