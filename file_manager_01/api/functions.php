<?php


function deleteDir($dir) {
    if (is_dir($dir)) {
      $objects = array_diff(scandir($dir),array('.','..'));      
      foreach ($objects as $object) {                   
          (is_dir($dir."/".$object)) ? deleteDir($dir."/".$object) :unlink($dir."/".$object);        
      }      
      return rmdir($dir);
    }
}

function CopyDir($sourcePath,$destinationPath) {
    if(!isset($fileNames)){
        $fileNames = [];
    }
    array_push($fileNames, basename($destinationPath));
    if (is_dir($sourcePath)) {        
        if( !file_exists($destinationPath) ){                
            mkdir(trim($destinationPath),0777);
        }else{
            $newName = rename_path($destinationPath);      
            
             mkdir(trim($newName), 0777);
             $destinationPath = $newName;
        }           
      $objects = array_diff(scandir($sourcePath),array('.','..'));      
      foreach ($objects as $object) {                   
        (is_dir($sourcePath."/".$object)) ? CopyDir($sourcePath."/".$object,$destinationPath."/".$object) :copy($sourcePath."/".$object,$destinationPath."/".$object);        
      }            
    }else{
        if(file_exists($destinationPath) ){                 
            $destinationPath = rename_path($destinationPath);                                        
        }           
        copy($sourcePath,$destinationPath);
    }
    return $fileNames;
}

function rename_path($path) {    

    if (!file_exists($path)) return $path;
    $fnameNoExt = pathinfo($path,PATHINFO_FILENAME);
    $ext = ( is_dir($path) ) ? ' ' : '.'. pathinfo($path, PATHINFO_EXTENSION);

    $i = 1;
    
    $path = ( !empty($ext) ) ?str_replace($ext," ",$path) : $path;

    while(file_exists("$path ($i)$ext")){        
        $i++;
    }     
    return ($ext) ? $path." ($i)$ext" : "$path ($i)" ;

}

?>