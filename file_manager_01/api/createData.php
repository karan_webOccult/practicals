<?php

    require('functions.php');

    $response = [];
    if(!empty($_POST['name'])){
         
        if($_POST['method'] == 'createFolder'){
            $filterText = "/^[a-zA-Z0-9()' ]*$/";          
        }else{
            $filterText = "/^[a-zA-Z0-9.()' ]*$/";
            if(!in_array( pathinfo($_POST['name'],PATHINFO_EXTENSION) ,['txt','jpeg','jpg','png','xls'])){
                $response['error'] = true;
                $response['message'] = 'Only txt / xls / png / jpeg and jpg extensions are allowed';
                header('Content-Type: application/json');
                echo json_encode($response);
                return;
            }

        }

        if(!preg_match($filterText, $_POST['name'])){        
            $response['error'] = true;
            $response['message'] = 'No special characters are allowed !';
        }else{

            $newName = rename_path($_POST['dirname'].'/'.$_POST['name']);               
            if( $_POST['method'] == 'createFolder') {                             
                mkdir( trim($newName), 0777);
            } else{
                $fp =  fopen($newName, "w");                
                fclose($fp);
            }
            $response['error'] = false;
            $text = ( $_POST['method'] == 'createFolder' ) ? 'Folder' : 'File';
            $response['data']['files'] = ['path'=>$newName,'type'=> ($text == 'Folder') ? 'folder' : pathinfo($newName,PATHINFO_EXTENSION),'name'=>basename($newName)];
            $response['message'] = $text.' created successfully';
            
        }
        
        header('Content-Type: application/json');
        echo json_encode($response);
    }



?>