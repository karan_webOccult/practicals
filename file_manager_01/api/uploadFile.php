<?php
require('functions.php');

function uploadFiles($file,$currentIndex,$dir){
    
    $file_path = $dir .'/'. basename($file['name'][$currentIndex]);
    $extension = pathinfo($file_path, PATHINFO_EXTENSION);    
    $allowedExtensions = ['jpg', 'png', 'gif', 'jpeg','zip','XLS'];
    $error = 0;
    $msg = '';
    if (!in_array($extension, $allowedExtensions)) {
      $error = 1;      
      $msg = count($file['name']) > 1 ? 'One of the file is having the extension which is not allowed.Please only upload files having JPG, PNG, GIF, XLS and ZIP extension' : 'The file have extension which is not allowed.Please only upload files having JPG, PNG, GIF, XLS and ZIP extension';
    }
    

    if(file_exists($file_path)){        
        //Get the new name if file already exist
        $file_path = rename_path($file_path);        
    }    
    $res['error'] = $error;
    $res['message'] = $msg;
    $res['data'] = [
        'path'=>$file_path,
        'type'=>$extension,
        'name' =>basename($file_path)
    ];
    return $res;
}

// print_r($_FILES);

if( isset($_FILES) && !empty($_FILES['file']['name'][0]) ){
   
    $fileInfo = [];
    $flag = 0;
    for ($i=0; $i < count($_FILES['file']['name']) ; $i++) {         
        $result = uploadFiles($_FILES['file'],$i,$_POST['dirname']);        
        if($result['error']){
            $flag=1;
            break;        
        }else{
            array_push($fileInfo,$result['data']);
            $_FILES['file']['name'][$i] = $result['data']['path'];
        }
    }
    if($flag){        
        $response['error'] = true;
        $response['message'] = $result['message'];      
    }else{
        //Upload the files if no error occured
        for ($i=0; $i < count($_FILES['file']['name']) ; $i++) {                                 
            move_uploaded_file($_FILES['file']['tmp_name'][$i],$_POST['dirname'].'/'.basename($_FILES['file']['name'][$i]));
        }
        $response['error'] = false;
        $response['message'] = 'File uploaded successfully !';
        $response['data']['files'] = $fileInfo;
    }
}else{
    $response['error'] = true;
    $response['message'] = 'This field is required';    
}

header('Content-Type: application/json');
echo json_encode($response);


?>