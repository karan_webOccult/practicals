<?php

function getFiles($dir){
    
    $files = array_diff(scandir($dir, SCANDIR_SORT_ASCENDING ),array('.','..'));
    $res = [];
    foreach( $files as $key => $value ){
        $temp = [];
        if(!is_dir($dir . '/' . $value ) ){
            if(findFileIcon(pathinfo($dir . '/' . $value, PATHINFO_EXTENSION ))){
                $temp['icon'] =  findFileIcon(pathinfo($dir . '/' . $value, PATHINFO_EXTENSION )); 
                $temp['name'] =  $value;
                $temp['path'] =  $dir.'/'.$value;
                $temp['type'] =  null;
                array_push($res,$temp);
            }        
        }else{
            
            $temp['icon'] =  findFileIcon('folder'); 
            $temp['name'] =  $value;
            $temp['path'] =  $dir.'/'.$value;
            $temp['type'] =  'folder';
            array_push($res,$temp);
        }
    }
    return $res;
}


function findFileIcon($type){

    $fileIcons = array(
        "folder"=>'assets/icons/folder.png',
        "txt"=>'assets/icons/txt.png',
        "jpeg"=>'assets/icons/jpg.png',
        "jpg"=>'assets/icons/jpg.png',
        "png"=>'assets/icons/png-icon.png',
        "xls"=>'assets/icons/xls.png');

    if(array_key_exists($type,$fileIcons)){        
        return $fileIcons[$type];
    }
    return false;
}

function deleteDir($dir) {
    if (is_dir($dir)) {
      $objects = array_diff(scandir($dir),array('.','..'));      
      foreach ($objects as $object) {                   
          (is_dir($dir."/".$object)) ? deleteDir($dir."/".$object) :unlink($dir."/".$object);        
      }      
      return rmdir($dir);
    }
}

function CopyDir($sourcePath,$destinationPath) {
    if (is_dir($sourcePath)) {        
        if( !file_exists($destinationPath) ){                
            mkdir($destinationPath,0700);
        }else{
             $newName = newName($destinationPath);   
             mkdir($newName);
             $destinationPath = $newName;
        }        
      $objects = array_diff(scandir($sourcePath),array('.','..'));      
      foreach ($objects as $object) {                   
          (is_dir($sourcePath."/".$object)) ? CopyDir($sourcePath."/".$object,$destinationPath."/".$object) :copy($sourcePath."/".$object,$destinationPath."/".$object);        
      }            
    }else{
        if(file_exists($destinationPath) ){     
            
            $destinationPath = newName($destinationPath);      
            
        }
        copy($sourcePath,$destinationPath);
    }
    return true;
}



function newName($path) {    
    if (!file_exists($path)) return $path;
    $fnameNoExt = pathinfo($path,PATHINFO_FILENAME);
    $ext = pathinfo($path, PATHINFO_EXTENSION);

    $i = 1;
    while(file_exists("$path ($i).$ext")) $i++;
    return ($ext) ? "$path ($i).$ext" : "$path ($i)" ;
}

if(isset($_POST) && $_POST['method'] == 'getData' ){    
    
    if(isset($_POST['dirname'])){
        $res = getFiles($_POST['dirname']);
    }    
    print_r(json_encode($res));

}

if(isset($_POST) && in_array($_POST['method'],array('createFolder','createFile')) ){

    $response = [];
    if(!empty($_POST['name'])){
         
        if($_POST['method'] == 'createFolder'){
            $filterText = "/^[a-zA-Z0-9()' ]*$/";          
        }else{
            $filterText = "/^[a-zA-Z0-9.()' ]*$/";
            if(!findFileIcon(pathinfo($_POST['name'], PATHINFO_EXTENSION ))){
                $response['error'] = true;
                $response['message'] = 'Only txt / xls / png / jpeg and jpg extensions are allowed';
                print_r(json_encode($response));
                return;
            }

        }

        if(!preg_match($filterText, $_POST['name'])){        
            $response['error'] = true;
            $response['message'] = 'No special characters are allowed !';
        }else{
            $newName = newName($_POST['dirname'].'/'.$_POST['name']);               
            if( $_POST['method'] == 'createFolder') {             
                mkdir($newName, 0700);
            } else{
                $fp =  fopen($newName, "w");                
                fclose($fp);
            }
            $response['error'] = false;
            $text = ( $_POST['method'] == 'createFolder' ) ? 'Folder' : 'File';
            $response['message'] = $text.' created successfully';
        }
        print_r(json_encode($response));
    }

}


if(isset($_POST) && in_array($_POST['method'],array('move','copy','delete')) ){
    
    $files = json_decode($_POST['name'],true);          
    $destinationPath = $_POST['dirname'];        

    if(in_array($_POST['method'],array('move','copy')) && is_dir($files[0]) ){
        
        if (strpos($destinationPath, $files[0]) !== false) {
            
            $response['error'] = false;
            $response['message'] = 'The destination folder is a subfolder of the source folder';
            print_r(json_encode($response));
            return;
        }

    }

    foreach( $files as $fileName ){
        $tmp = explode('/',$fileName);                
        
        if(in_array($_POST['method'],array('move','copy'))){
            $flag = ($_POST['method'] == 'move')  ? rename($fileName, $destinationPath .'/'. $tmp[ count($tmp) - 1 ] ) : CopyDir($fileName, $destinationPath .'/'. $tmp[ count($tmp) - 1 ] );
        }else{
            $filePath = $destinationPath.'/'.$tmp[ count($tmp) - 1 ];
            
            if(is_dir($filePath)){              
                $flag = deleteDir($filePath);                       
            }else{
                $flag = unlink( $filePath );   
            }                       
        }      
    }

    if($flag){
        $response['error'] = false;
        $response['message'] = 'Opeation successful!';
    }else{
        $response['error'] = true;
        $response['message'] = 'Something went wrong.';
    }
    print_r(json_encode($response));

}

if(isset($_POST) && in_array($_POST['method'],array('rename')) ){

    $filterText = "/^[a-zA-Z0-9' ]*$/";   
    $files = json_decode($_POST['name'],true);           
    $ext =  !is_dir($files[0])  ? '.'.pathinfo($files[0], PATHINFO_EXTENSION ) : '';
    $destinationPath = $_POST['dirname'].'/'.$_POST['newName'] .$ext ;        
    if(!preg_match($filterText, $_POST['newName'])){        
        $response['error'] = true;
        $response['message'] = 'No special characters are allowed !';
    }else{
        if(rename($files[0],$destinationPath)){
            $response['error'] = false;
            $response['message'] = 'Opeation successful!';
        }else{
            $response['error'] = true;
            $response['message'] = 'Something went wrong.';
        }
    }
    print_r(json_encode($response));
}

?>