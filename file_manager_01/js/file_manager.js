var currentActiveDirectory = '';
var currentSelectedFiles = [],
    tempSelectedFiles = [];
var currentActiveMethod = '';

//get data from api
function get_dir_list(dirname = '../root', currElement = '', clearExistingOutput = true) {
    var currElement = currElement;
    $.ajax({
        url: 'api/getData.php',
        method: 'POST',
        data: { dirname: dirname, method: 'getData' },
        dataType: 'json',
        success: function(result) {
            set_dir(result.data, dirname, currElement, clearExistingOutput);
            $.each(tempSelectedFiles, function name(k, v) {
                $(".right-content").find("[data-path='" + v.path + "']").addClass('opacity-05');
            });
        }
    });
}

//Create folders and files 
function set_dir(result, dirname, currElement, clearExistingOutput) {
    currentSelectedFiles = [];
    currentActiveDirectory = dirname;

    (dirname == '../root' && clearExistingOutput) ? $('.accordion').html(''): '';
    (dirname != '../root' && currElement) ? $(currElement).siblings().html(''): '';
    (clearExistingOutput) ? $('.right-content .container .file').remove(): '';
    $('#back').attr('data-path', (dirname == '../root') ? '' : currentActiveDirectory.substring(0, currentActiveDirectory.lastIndexOf('/')));
    $.each(result, function(key, val) {
        $.each(val, function(k, v) {
            if (!$.isEmptyObject(v)) {
                var tmp = $('.accordion-item:first').clone().find('span').html(v.name).parent().attr({ 'data-path': v.path, 'data-type': v.type }).closest('.accordion-item')[0];
                var tmp1 = $('.right-content >.file').clone().find('span').html(v.name).parent().attr({ 'data-path': v.path, 'data-type': v.type })[0];
                $('.right-content > .container').append(tmp1);
                if (dirname == '../root') {
                    $('.accordion').append(tmp);
                    $('.right-content > .container').append(tmp1);
                } else {
                    (currElement) ? $(currElement).siblings().append(tmp): $('.right-content > .container').append(tmp1);
                    (!currElement) ? $('.accordion').append(tmp): '';
                }
            }
        });
    });
}

//Remove the file or folder from the specified path
function removeElements(filePath) {
    $.each(filePath, function name(k, v) {
        $('.right-content').find("[data-path='" + v.path + "']").remove();
        $('.accordion-item').find("[data-path='" + v.path + "']").parent().remove();
    });
}

//One function for all the methods like cut,copy,paste,create file and folder
function fileOperations(filename, method, newName = '', url = 'api/setData.php') {
    data = { dirname: currentActiveDirectory, name: filename, method: method };
    (method == 'rename') ? data.newName = newName: '';
    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        dataType: 'json',
        success: function(result) {
            if (result.error) {
                bootbox.alert(result.message);
            } else {
                bootbox.alert(result.message);
                if (method == 'rename') {
                    var tmpExtension = (JSON.parse(filename)[0].type.toLowerCase() != 'folder') ? '.' + JSON.parse(filename)[0].type : '';
                    $("[data-path='" + JSON.parse(filename)[0].path + "']").attr('data-path', currentActiveDirectory + '/' + newName + '.' + $("[data-path='" + JSON.parse(filename)[0].path + "']").attr('data-type')).find('span').html(newName + tmpExtension);
                }
                if (method == 'copy' || method == 'move') {
                    (method == 'move') ? $('#paste-file').addClass('opacity-05'): '';
                    $('.general-right-click [data-action="paste"]').removeClass('active');
                    console.log(JSON.parse(filename));
                    get_dir_list(currentActiveDirectory);
                }
                if (method == 'createFolder' || method == 'createFile') {
                    $.each(result.data, function name(k, v) {
                        var tmp = $('.accordion-item:first').clone().find('span').html(v.name).parent().attr({ 'data-path': v.path, 'data-type': v.type }).closest('.accordion-item')[0];
                        var tmp1 = $('.right-content >.file').clone().find('span').html(v.name).parent().attr({ 'data-path': v.path, 'data-type': v.type })[0];
                        console.log(tmp, tmp1, $('.right-content > .container'), $('.accordion .active:last'));
                        $('.right-content > .container').append(tmp1);
                        (currentActiveDirectory == '../root') ? $('.accordion').append(tmp): $('.accordion .active:last').find('.paragraph').append(tmp);
                    });
                }
            }

        }
    });
}

//Function for taking user input and processing the data
function prompt(callback, method) {
    if (method == 'rename') {
        var type = (tempSelectedFiles[0].type != 'folder') ? tempSelectedFiles[0].name.substring(0, tempSelectedFiles[0].name.lastIndexOf('.')) : tempSelectedFiles[0].name;
    }
    var val = (method == 'rename') ? type : '';
    bootbox.prompt({
        title: "Enter New Name ",
        centerVertical: true,
        inputType: 'text',
        value: val,
        callback: function(result) {
            if (result) {
                if (method == 'rename') {
                    //calling the fileoperations method
                    callback(JSON.stringify(tempSelectedFiles), method, result);
                } else {

                    //$('.accordion > .active').find('.title:first').trigger('click', { isAlreadyOpened: '1' });

                    callback(result, method, '', 'api/createData.php');
                }
            } else {
                tempSelectedFiles = [];
            }
        },
    });
}

//on dbclick event
$(document).on('dblclick', '.file', function(e, data) {
    if ($(this).data('type') && $(this).data('type') == 'folder') {
        get_dir_list($(this).data('path'), this);
        $("[data-path='" + $(this).data('path') + "']").trigger('click');
    }
});

$(document).on('click', '.file', function(e) {
    if (e.metaKey || e.ctrlKey) {
        //Execute it when user has pressed ctrl key
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            currentSelectedFiles.push({ path: $(this).attr('data-path'), type: $(this).attr('data-type'), name: $(this).find('span').html() });
        } else {
            currentSelectedFiles.splice(currentSelectedFiles.findIndex(x => x.path === $(this).attr('data-path')), 1);
        }
    } else {
        $(this).toggleClass('active').siblings().removeClass('active');
        currentSelectedFiles = [];
        ($(this).hasClass('active')) ? currentSelectedFiles.push({ path: $(this).attr('data-path'), type: $(this).attr('data-type'), name: $(this).find('span').html() }): '';
    }
});

$(document).on('click', 'body', function(e, data) {
    //Execute it when user clicks inside of body
    if ($('.file').find(e.target).length == 0) {
        currentSelectedFiles = [];
        $('.right-content .container .active').removeClass('active');
        $('.right-click,.general-right-click').hide();
    }
});

$(document).on('click', '.title', function(e, data) {
    //Used for accordian menu
    if ($(this).data('type') && $(this).data('type') == 'folder') {
        get_dir_list($(this).data('path'), this);
        if ($(this).hasClass('title')) {
            var currElement = $(this).parent();
            var parentOfCurrentElement = currElement.parent();
            var parentClassName = currElement.attr('class');
            parentOfCurrentElement.find('.' + parentClassName).not(currElement).removeClass('active');
            (!data) ? $(currElement).toggleClass("active"): '';
        }
    }
});

$(document).on('click', '#home', function(e, data) {
    if (currentActiveDirectory != '../root') {
        get_dir_list();
    }
});

$(document).on('click', '#back', function() {
    //Get the user back to the previous directory
    if ($(this).attr('data-path') != '') {
        $(this).attr('data-path', $(this).attr('data-path'));
        get_dir_list($(this).attr('data-path'), '');
        $("[data-path='" + currentActiveDirectory + "']").parent().removeClass('active');
    } else {
        bootbox.alert("You don't have the right to go back");
    }
});

$(document).on('click', '#move-file,#copy-file,#paste-file', function() {

    if ($(this).attr('id') == 'paste-file') {
        if (currentActiveMethod) {
            //Calling the method for paste operation
            fileOperations(JSON.stringify(tempSelectedFiles), currentActiveMethod);
            tempSelectedFiles = [];
            $('#paste-file').addClass('opacity-05');
        }
    } else {
        var str = ($(this).attr('id') == 'move-file') ? 'move' : 'copy';
        if ($.isEmptyObject(currentSelectedFiles)) {
            bootbox.alert('Please select a file to ' + str);
        } else {
            tempSelectedFiles = currentSelectedFiles;
            $(".right-content").find('.opacity-05').removeClass('opacity-05');
            if (str == 'move') {
                $.each(tempSelectedFiles, function name(k, v) {
                    $(".right-content").find("[data-path='" + v.path + "']").addClass('opacity-05');
                });
            }
            currentActiveMethod = str;
            $('#paste-file').removeClass('opacity-05');
            $('.general-right-click [data-action="paste"]').addClass('active');
        }
    }
});

$(document).on('click', '#delete', function() {
    //Used for deleting the files and folders   
    var tmp = currentSelectedFiles;
    if (currentSelectedFiles.length > 0) {
        bootbox.confirm("Are you sure you want to delete !", function(result) {
            if (result) {
                fileOperations(JSON.stringify(tmp), 'delete');
                removeElements(tmp);
            }
        });
    } else {
        bootbox.alert('Please select a file to delete');
    }
});

$(document).on('click', '#create,#add-file', function() {
    //Used for creating the files and folder
    var methodType = ($(this).attr('id') == 'add-file') ? 'createFile' : 'createFolder';
    prompt(fileOperations, methodType);
});

$(document).bind("contextmenu", function(event) {
    //Show menu on right click
    event.preventDefault();
    if ($('.file').find(event.target).length > 0) {
        if (event.metaKey || event.ctrlKey) {
            tempSelectedFiles = currentSelectedFiles;
        } else {
            if (!$('.file').find(event.target).closest('.file').hasClass('active')) {
                currentSelectedFiles = [];
                $('.file').find(event.target).closest('.file').toggleClass('active').siblings().removeClass('active');
                ($('.file').find(event.target).closest('.file').hasClass('active')) ? currentSelectedFiles.push({ path: $('.file').find(event.target).closest('.file').attr('data-path'), type: $('.file').find(event.target).closest('.file').attr('data-type'), name: $('.file').find(event.target).closest('.file').find('span').html() }): '';
                tempSelectedFiles = currentSelectedFiles;
            }
        }
        var element = $('.right-click');
    } else {
        var element = $(".general-right-click");
    }
    element.finish().toggle(100).css({ top: event.pageY + "px", left: event.pageX + "px" });
});

$(document).bind("mousedown", function(e) {
    //Hide the menu if opened
    (!$(e.target).parents(".general-right-click").length > 0) ? $(".general-right-click").hide(100): '';
    (!$(e.target).parents(".right-click").length > 0) ? $(".right-click").hide(100): ''
});

$(".general-right-click li").click(function() {
    //Trigger events for each menu options
    switch ($(this).attr("data-action")) {
        case "create":
            $('#create').trigger('click');
            break;
        case "paste":
            $('#paste-file').trigger('click');
            break;
        case "add-file":
            $('#add-file').trigger('click');
            break;
    }
    $(".general-right-click").hide(100);
});

$(".right-click li").click(function() {
    //Trigger events for each menu options
    switch ($(this).attr("data-action")) {
        case "copy":
            $('#copy-file').trigger('click');
            break;
        case "cut":
            $('#move-file').trigger('click');
            break;
        case "delete":
            $('#delete').trigger('click');
            break;
        case "rename":
            prompt(fileOperations, 'rename');
            break;
    }
    $(".general-right-click").hide(100);
});

var myDropzone;
//Open dialog box for uploading files
$(document).on('click', '#upload-file', function() {
    var dialog = bootbox.dialog({
        title: 'Upload Files',
        size: 'large',
        centerVertical: true,
        message: '<div id="uploadFiles" class="dropzone"></div> <progress id="progress" class="w90" value="0" max="100"></progress> <span class="percentage"> <b>0</b> % </span> <p id="responseMessage" class="errorMessage" class="pt10"></p> <div class="text-right pt10"><button type="submit" id="uploadBtn"> Upload Files </button></div>',
    }).on("shown.bs.modal", function() {
        //Called dropzone when modal is loaded
        myDropzone = new Dropzone("#uploadFiles", {
            url: "api/uploadFile.php",
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 30,
            maxFilesize: 5,
            maxFiles: 50,
            addRemoveLinks: true,
            acceptedFiles: ".zip,.jpeg,.jpg,.png,.txt,.xls",
            init: function() {
                //Sending post data with file data
                this.on("sendingmultiple", function(file, xhr, formData) {
                    formData.append("dirname", currentActiveDirectory);
                })

            },
            successmultiple: function(file, response) {
                //After success get the response
                if (!response.error) {
                    set_dir(response.data, currentActiveDirectory, '', false);
                    $('#responseMessage').attr('class', 'successMessage').html('Files uploaded successfully').show();
                } else {
                    $('#responseMessage').attr('class', 'errorMessage').html(response.message).show();
                }
            },
            totaluploadprogress: function(a, b, c) {
                console.log($('#progress').val(a).next().find('b').html(a));
            }
        });
    });
});

$(document).on('click', '#uploadBtn', function(e) {
    e.preventDefault();
    if (myDropzone.files.length == 0) {
        $('#responseMessage').attr('class', 'errorMessage').html('Please select or drag any file to upload').show();
    } else if (myDropzone.getRejectedFiles().length > 0) {
        $('#responseMessage').attr('class', 'errorMessage').html('Please remove all files having errors to proceed further').show();
    } else {
        $('#progress').show().next().show();
        myDropzone.processQueue();
    }
});

get_dir_list()