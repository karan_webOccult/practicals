var currentActiveDirectory = '';
var currentSelectedFiles = [],
    tempSelectedFiles = [];
var currentActiveMethod = '';

function get_dir_list(dirname = 'root', currElement = '') {
    var currElement = currElement;
    $.ajax({
        url: 'operations.php',
        method: 'POST',
        data: {
            dirname: dirname,
            method: 'getData'
        },
        dataType: 'json',
        success: function(result) {

            set_dir(result, dirname, currElement);

        }
    });
}

function set_dir(result, dirname, currElement) {
    currentSelectedFiles = [];
    currentActiveDirectory = dirname;
    (dirname == 'root') ? $('.accordion').html(''): '';
    (dirname != 'root') ? $(currElement).siblings().html(''): '';

    $('.right-content .container .file').remove();
    $('#back').attr('data-path', (dirname == 'root') ? '' : currentActiveDirectory.substring(0, currentActiveDirectory.lastIndexOf('/')));

    $.each(result, function(k, v) {

        if (!$.isEmptyObject(v)) {

            var tmp = $('.accordion-item:first').clone().find('.title:first > img').attr(
                'src', v.icon
            ).
            siblings('span').html(v.name).parent().attr({
                'data-path': v.path,
                'data-type': v.type
            }).closest('.accordion-item')[0];

            var tmp1 = $('.right-content >.file').clone().find('img').attr('src', v.icon).parent().siblings('span').html(v.name).parent().attr({
                'data-path': v.path,
                'data-type': v.type
            })[0];

            if (dirname == 'root') {
                $('.accordion').append(tmp);
                $('.right-content > .container').append(tmp1);

            } else {

                $(currElement).siblings().append(tmp);
                $('.right-content > .container').append(tmp1);

            }
        }
    });
}

$(document).on('dblclick', '.file', function(e, data) {
    if ($(this).data('type') && $(this).data('type') == 'folder') {
        get_dir_list($(this).data('path'), this);
    }
});

$(document).on('click', '.file', function(e) {
    if (e.metaKey || e.ctrlKey) {

        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            currentSelectedFiles.push($(this).attr('data-path'));
        } else {
            currentSelectedFiles.splice($.inArray($(this).attr('data-path'), currentSelectedFiles), 1);
        }

    } else {

        $(this).toggleClass('active').siblings().removeClass('active');
        currentSelectedFiles = [];
        ($(this).hasClass('active')) ? currentSelectedFiles.push($(this).attr('data-path')): '';
    }

});

$(document).on('click', 'body', function(e, data) {

    if ($('.file').find(e.target).length == 0) {
        currentSelectedFiles = [];
        $('.right-content .container .active').removeClass('active');
        console.log('On BG Click', currentSelectedFiles);
        $('.right-click,.general-right-click').hide();
    } else {
        // $('.right-content .container .active').removeClass('active');
        // currentSelectedFiles = [];
    }

});

$(document).on('click', '.title', function(e, data) {
    if ($(this).data('type') && $(this).data('type') == 'folder') {
        get_dir_list($(this).data('path'), this);
        if ($(this).hasClass('title')) {
            var currElement = $(this).parent();
            var parentOfCurrentElement = currElement.parent();
            var parentClassName = currElement.attr('class');
            parentOfCurrentElement.find('.' + parentClassName).not(currElement).removeClass('active').find('.fa').removeClass("rotate-up");
            $(currElement).toggleClass("active").find('.fa:first').toggleClass("rotate-up");

        }
    }
});

$(document).on('click', '#home', function(e, data) {
    get_dir_list();
});

$(document).on('click', '#back', function() {
    if ($(this).attr('data-path') != '') {
        $(this).attr('data-path', $(this).attr('data-path'));
        get_dir_list($(this).attr('data-path'), '');
    } else {
        bootbox.alert("You don't have the right to go back");
    }
});

$(document).on('click', '#move-file,#copy-file,#paste-file', function() {

    if ($(this).attr('id') == 'paste-file') {
        if (currentActiveMethod) {
            fileOperations(JSON.stringify(tempSelectedFiles), currentActiveMethod);
        }
    } else {

        var str = ($(this).attr('id') == 'move-file') ? 'move' : 'copy';
        get_dir_list();
        if ($.isEmptyObject(currentSelectedFiles)) {
            bootbox.alert('Please select a file to ' + str);
        } else {

            tempSelectedFiles = currentSelectedFiles;
            currentActiveMethod = str;
            $('#paste-file').removeClass('opacity-05');

            $('.general-right-click [data-action="paste"]').addClass('active');
        }

    }
});

$(document).on('click', '#delete', function() {
    var tmp = currentSelectedFiles;
    if (currentSelectedFiles.length > 0) {
        bootbox.confirm("Are you sure you want to delete !", function(result) {
            if (result) {
                fileOperations(JSON.stringify(tmp), 'delete');
            }
        });

    } else {
        bootbox.alert('Please select a file to delete');
    }
});

$(document).on('click', '#create,#add-file', function() {
    var methodType = ($(this).attr('id') == 'add-file') ? 'createFile' : 'createFolder';
    bootbox.prompt({
        title: "Enter Name ",
        centerVertical: true,
        callback: function(result) {
            if (result) {
                fileOperations(result, methodType)
            }

        }
    });
});

function fileOperations(filename, method, newName = '') {

    data = {
        dirname: currentActiveDirectory,
        name: filename,
        method: method
    };

    if (method == 'rename') {
        data.newName = newName;
    }
    $.ajax({
        url: 'operations.php',
        method: 'POST',
        data: data,
        dataType: 'json',
        success: function(result) {
            if (method == 'copy' || method == 'move') {
                $('#paste-file').addClass('opacity-05');
                $('.general-right-click [data-action="paste"]').removeClass('active');
            }
            if (result.error) {
                bootbox.alert(result.message);
            } else {
                bootbox.alert(result.message);
                get_dir_list(currentActiveDirectory, '')
            }
        }
    });
}

$(document).bind("contextmenu", function(event) {

    event.preventDefault();

    if ($('.file').find(event.target).length > 0) {

        if (event.metaKey || event.ctrlKey) {
            tempSelectedFiles = currentSelectedFiles;
        } else {
            if (!$('.file').find(event.target).closest('.file').hasClass('active')) {
                $('.file').find(event.target).closest('.file').toggleClass('active').siblings().removeClass('active');
                currentSelectedFiles = [];
            }
            tempSelectedFiles = currentSelectedFiles;
            ($('.file').find(event.target).closest('.file').hasClass('active')) ? currentSelectedFiles.push($('.file').find(event.target).closest('.file').attr('data-path')): '';
        }
        var element = $('.right-click');
    } else {

        var element = $(".general-right-click");
    }

    element.finish().toggle(100).
    css({
        top: event.pageY + "px",
        left: event.pageX + "px"
    });

});

$(document).bind("mousedown", function(e) {

    if (!$(e.target).parents(".general-right-click").length > 0) {
        $(".general-right-click").hide(100);
    }
    if (!$(e.target).parents(".right-click").length > 0) {
        $(".right-click").hide(100);
    }
});

$(".general-right-click li").click(function() {

    switch ($(this).attr("data-action")) {
        case "create":
            $('#create').trigger('click');
            break;
        case "paste":
            $('#paste-file').trigger('click');
            break;
        case "add-file":
            $('#add-file').trigger('click');
            break;
    }
    $(".general-right-click").hide(100);
});

$(".right-click li").click(function() {

    switch ($(this).attr("data-action")) {

        case "copy":
            $('#copy-file').trigger('click');
            break;
        case "cut":
            $('#move-file').trigger('click');
            break;
        case "delete":
            $('#delete').trigger('click');
            break;
        case "rename":
            alert('Called');
            promptRename("Enter New name 2", fileOperations);

            break;
    }
    $(".general-right-click").hide(100);
});

function promptRename($title, callback) {
    bootbox.prompt({
        title: title,
        centerVertical: true,
        callback: function(result) {
            if (result) {
                callback(JSON.stringify(tempSelectedFiles), 'rename', result);
            }
        }
    });
}

get_dir_list();