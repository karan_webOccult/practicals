<?php
date_default_timezone_set("Asia/kolkata");

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Date Functions</title>
</head>
<body>
   <form action='#' method='post'>
        <h1>Compare Two Dates</h1>
        <div>Date 1 <div> <input type='date' name='date_1' value='<?= isset( $_POST['date_1'] ) ? $_POST['date_1'] : '' ?>' > </div></div>
    
        <div>Date 2 <div> <input type='date' name='date_2' value="<?= isset( $_POST['date_2'] ) ? $_POST['date_2'] : '' ?>"> </div></div>
        <br>
        <div> <input type='submit' name='submit'> </div>
   </form>
    <?php
    if($_POST){
        $difference = (  strtotime($_POST['date_2']) >  strtotime($_POST['date_1']) ) ? strtotime($_POST['date_2']) - strtotime($_POST['date_1'])   :  strtotime($_POST['date_1']) - strtotime($_POST['date_2']);
        $date_result = getdate($difference) ;    
        echo '<br>Date 1 : '. date('Y-M-d (l)',strtotime($_POST["date_1"])).'<br> Date 2 :'.date('Y-M-d (l)',strtotime($_POST["date_2"]));
        echo '<br> Remaining Days : '.$date_result['yday'];
        echo '<br> Remaining Hours : '.intval($date_result['yday'] * 24);
        echo '<br> Remaining Minutes : '.intval($date_result['yday'] * 24 * 60);
        echo '<br> Remaining Seconds : '.intval($date_result['yday'] * 24 * 3600) ;
        echo '<br><br> The next day after '.date('Y-M-d ',strtotime($_POST["date_2"])).' will be on '.date('Y-m-d', strtotime('tomorrow',strtotime($_POST['date_2'])) );
        echo '<br> The previous day after '.date('Y-M-d ',strtotime($_POST["date_2"])).' will be on '.date('Y-m-d', strtotime('yesterday',strtotime($_POST['date_2'])) );
        echo '<br> The next sunday after '.date('Y-M-d ',strtotime($_POST["date_2"])).' will be on '.date('Y-m-d', strtotime('next sunday',strtotime($_POST['date_2'])) );
        echo '<br> The next saturday after '.date('Y-M-d ',strtotime($_POST["date_2"])).' will be on '.date('Y-m-d', strtotime('next saturday',strtotime($_POST['date_2'])) );
    }    
    ?>
</body>
</html>